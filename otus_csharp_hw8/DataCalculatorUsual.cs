﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw8
{
    public class DataCalculatorUsual : IDataСalculator
    {
        private long TimeWork;

        public DataCalculatorUsual()
        {
        }

        public long Sum(int[] dataInts, int countDataRows)
        {

            long res;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            res = dataInts.Sum();

            stopwatch.Stop();
            TimeWork = stopwatch.ElapsedMilliseconds;

            return res;
        }

        public string ShowInfoWork()
        {
            return $"Время вычисления суммы с помощью обычного способа: {TimeWork} ms.";
        }

    }
}
