﻿namespace otus_csharp_hw8
{
    internal class Program
    {

        public static int countThread = 4;
        public static int countElemrnts = 100000;
        public static int[] ints;

        static void Main(string[] args)
        {

            Console.WriteLine(" *** Параллельная обработка данных ***");

            ConsoleKey choice;
            while (true)
            {
                SetSettings();

                GenerateData();

                ProcessingData();

                Console.Write("Повторить загрузку с другими настройками? (y/n):");
                choice = Console.ReadKey(true).Key;
                Console.WriteLine(choice.ToString());
                if (choice != ConsoleKey.Y)
                {
                    break;
                }
            }
        }

        static void GenerateData()
        {
            Console.WriteLine("Генерация данных ...");

            Random _random = new Random();

            ints = new int[countElemrnts];

            ints = Enumerable.Range(0, countElemrnts).Select(s => _random.Next(0, 9)).ToArray();

            Console.WriteLine("Данные сгенерированны!");
        }

        public static void ProcessingData()
        {
            Console.WriteLine("Обработка данных. Подготовка потоков");
            Console.WriteLine($"Количество элементов: {countElemrnts}");

            Console.WriteLine("Вычисление суммы элементов массива интов");
            long res;

            // Обычное вычисление
            IDataСalculator calcUsual = new DataCalculatorUsual();
            res = calcUsual.Sum(ints, countElemrnts);
            Console.WriteLine($"Сумма элементов {res}");
            Console.WriteLine(calcUsual.ShowInfoWork());

            // Параллельное вычисление с помощью Thread
            IDataСalculator calcThread = new DataCalculatorThread(countThread);
            res = calcThread.Sum(ints, countElemrnts);
            Console.WriteLine($"Сумма элементов {res}");
            Console.WriteLine(calcThread.ShowInfoWork());

            // Параллельное вычисление с помощью LINQ
            IDataСalculator calcPLINQ = new DataCalculatorPLINQ(countThread);
            res = calcPLINQ.Sum(ints, countElemrnts);
            Console.WriteLine($"Сумма элементов {res}");
            Console.WriteLine(calcPLINQ.ShowInfoWork());
        }

        static void SetSettings()
        {
            ConsoleKey choice;

            Console.WriteLine($"Количество элементов в массиве интов: {countElemrnts}");
            Console.Write("Изменить элементов? (y/n):");
            choice = Console.ReadKey(true).Key;
            Console.WriteLine(choice.ToString());
            switch (choice)
            {
                case ConsoleKey.Y: SetIntValueConsole(out countElemrnts); break;
                default: break;
            }

            Console.WriteLine($"Количество потоков: {countThread}");
            Console.Write("Изменить количество потоков? (y/n):");
            choice = Console.ReadKey(true).Key;
            Console.WriteLine(choice.ToString());
            switch (choice)
            {
                case ConsoleKey.Y: SetIntValueConsole(out countThread); break;
                default: break;
            }

        }

        static void SetIntValueConsole(out int value)
        {
            while (true)
            {
                Console.Write($"Введите число: ");
                string? input = Console.ReadLine();
                if (int.TryParse(input, out int number))
                {
                    value = number;
                    Console.WriteLine();
                    return;
                }
                {
                    Console.WriteLine("Не корректное число!");
                }
            }
        }


    }
}