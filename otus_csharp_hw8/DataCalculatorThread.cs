﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw8
{

    public class DataCalculatorThread : IDataСalculator
    {
        private int CountThread;
        private long TimeWork;

        public DataCalculatorThread(int countThread)
        {
            CountThread = countThread;
        }

        public long Sum(int[] dataInts, int CountElements)
        {

            long res;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            List<Thread> threads = new List<Thread>();
            List<ThreadProcessingHandler> Handlers = new List<ThreadProcessingHandler>();

            for (int i = 0; i < CountThread; i++)
            {
                Handlers.Add(new ThreadProcessingHandler(i + 1, dataInts, (i * CountElements / CountThread), CountElements / CountThread));
                threads.Add(new Thread(Handlers[i].Handle));
                threads[i].Start();
            }

            for (int i = 0; i < CountThread; i++)
            {
                threads[i].Join();
            }

            res = Handlers.Sum(x => x.Result);

            stopwatch.Stop();
            TimeWork = stopwatch.ElapsedMilliseconds;

            return res;
        }

        public string ShowInfoWork()
        {
            return $"Время вычисления с помощью {CountThread} потоков (Thread): {TimeWork} ms.";
        }

    }

    public class ThreadProcessingHandler
    {
        private int NumberThread;
        private int StartIndex;
        private int EndIndex;
        private int CountElements;
        private int[] Ints;
        public long Result;
        public ManualResetEvent doneEvent;

        public ThreadProcessingHandler(int numberThread, int[] ints, int startIndex, int countElements)
        {
            NumberThread = numberThread;
            StartIndex = startIndex;
            EndIndex = startIndex + countElements;
            CountElements = countElements;
            Ints = ints;
            doneEvent = new ManualResetEvent(false);
        }

        public void Handle()
        {
            // Console.WriteLine($"Поток {NumberThread}. Вычисление суммы...");
            int[] IntsRange = Ints[StartIndex..EndIndex];
            Result = IntsRange.Sum();
            // Console.WriteLine($"Поток {NumberThread}. Обработка завершена!");
            doneEvent.Set();
        }
    }

}
