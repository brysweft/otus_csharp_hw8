﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw8
{
    public class DataCalculatorPLINQ : IDataСalculator
    {
        private int CountThread;
        private long TimeWork;

        public DataCalculatorPLINQ(int countThread)
        {
            CountThread = countThread;
        }

        public long Sum(int[] dataInts, int CountElements)
        {

            long res;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            int[] number_threads = Enumerable.Range(1, CountThread).ToArray();

            var resulrs = from n  in number_threads.AsParallel() select Handle(n, dataInts, ((n - 1) * CountElements / CountThread), CountElements / CountThread);

            res = resulrs.Sum();

            stopwatch.Stop();
            TimeWork = stopwatch.ElapsedMilliseconds;

            return res;
        }
        public long Handle(int numberThread, int[] ints, int startIndex, int countElements)
        {
            // Console.WriteLine($"Поток {numberThread}. Вычисление суммы...");
            int endIndex = startIndex + countElements;
            int[] IntsRange = ints[startIndex..endIndex];
            long res = IntsRange.Sum();
            // Console.WriteLine($"Поток {numberThread}. Обработка завершена!");
            return res;
        }

        public string ShowInfoWork()
        {
            return $"Время вычисления с помощью {CountThread} потоков (PLINQ): {TimeWork} ms.";
        }

    }
}
