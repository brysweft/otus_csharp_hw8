﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw8
{
    public interface IDataСalculator
    {
        public long Sum(int[] dataInts, int countDataRows);
        public string ShowInfoWork();

    }
}
